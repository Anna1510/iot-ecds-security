//
// Created by Анна Еловикова on 15.08.2021.
//

#include "Stribog.h"
#include <stdio.h>

void XTransformation(const BigInt_t* a, const BigInt_t* b, BigInt_t* result) {
    result->_integer[0] = a->_integer[0] ^ b->_integer[0];
    result->_integer[1] = a->_integer[1] ^ b->_integer[1];
    result->_integer[2] = a->_integer[2] ^ b->_integer[2];
    result->_integer[3] = a->_integer[3] ^ b->_integer[3];
    result->_integer[4] = a->_integer[4] ^ b->_integer[4];
    result->_integer[5] = a->_integer[5] ^ b->_integer[5];
    result->_integer[6] = a->_integer[6] ^ b->_integer[6];
    result->_integer[7] = a->_integer[7] ^ b->_integer[7];
}

void STransformation(const BigInt_t* state, BigInt_t* result) {
    uint_least8_t p[8];
    for(uint_fast8_t i = 0; i < 8; ++i) {
        p[0] = SBox[(state->_integer[i] & 0x00000000000000ff)];
        p[1] = SBox[(state->_integer[i] & 0x000000000000ff00) >> 8];
        p[2] = SBox[(state->_integer[i] & 0x0000000000ff0000) >> 16];
        p[3] = SBox[(state->_integer[i] & 0x00000000ff000000) >> 24];
        p[4] = SBox[(state->_integer[i] & 0x000000ff00000000) >> 32];
        p[5] = SBox[(state->_integer[i] & 0x0000ff0000000000) >> 40];
        p[6] = SBox[(state->_integer[i] & 0x00ff000000000000) >> 48];
        p[7] = SBox[(state->_integer[i] & 0xff00000000000000) >> 56];
        result->_integer[i] = (uint_least64_t)(*(uint_least64_t*)&p[0]);
    }
}

uint_least64_t PerformTauSubstitution(const BigInt_t* state, uint_fast8_t part) {
    uint_least8_t p[8];
    p[7] = GetByte(state, Tau[part]);
    p[6] = GetByte(state, Tau[part + 1]);
    p[5] = GetByte(state, Tau[part + 2]);
    p[4] = GetByte(state, Tau[part + 3]);
    p[3] = GetByte(state, Tau[part + 4]);
    p[2] = GetByte(state, Tau[part + 5]);
    p[1] = GetByte(state, Tau[part + 6]);
    p[0] = GetByte(state, Tau[part + 7]);
    return (uint_least64_t)(*(uint_least64_t*)&p[0]);
}

void PTransformation(const BigInt_t* state, BigInt_t* result) {
    BigInt_t temp;
    temp._integer[7] = PerformTauSubstitution(state, 0);
    temp._integer[6] = PerformTauSubstitution(state, 8);
    temp._integer[5] = PerformTauSubstitution(state, 16);
    temp._integer[4] = PerformTauSubstitution(state, 24);
    temp._integer[3] = PerformTauSubstitution(state, 32);
    temp._integer[2] = PerformTauSubstitution(state, 40);
    temp._integer[1] = PerformTauSubstitution(state, 48);
    temp._integer[0] = PerformTauSubstitution(state, 56);

    result->_integer[7] = temp._integer[7];
    result->_integer[6] = temp._integer[6];
    result->_integer[5] = temp._integer[5];
    result->_integer[4] = temp._integer[4];
    result->_integer[3] = temp._integer[3];
    result->_integer[2] = temp._integer[2];
    result->_integer[1] = temp._integer[1];
    result->_integer[0] = temp._integer[0];
}

void AddModulo512(const BigInt_t* a, const BigInt_t* b, BigInt_t* result) {
    uint_least64_t carry = 0;
    uint_least64_t sum;

    for (int i = 7; i >= 0; --i) {
        sum = a->_integer[i] + b->_integer[i] + carry;
        carry = (sum < a->_integer[i]);
        result->_integer[i] = sum;
    }
}

void LTransformation(const BigInt_t* state, BigInt_t* result) {
    uint_least64_t _helper = 0;

    for (uint_fast8_t i = 0; i < 8; ++i) {
        for (int j = 0; j < 64; j += 8) {
            _helper ^= ((state->_integer[i] >> j) & 1)*A[63 - j];
            _helper ^= ((state->_integer[i] >> (j + 1)) & 1)*A[62 - j];
            _helper ^= ((state->_integer[i] >> (j + 2)) & 1)*A[61 - j];
            _helper ^= ((state->_integer[i] >> (j + 3)) & 1)*A[60 - j];
            _helper ^= ((state->_integer[i] >> (j + 4)) & 1)*A[59 - j];
            _helper ^= ((state->_integer[i] >> (j + 5)) & 1)*A[58 - j];
            _helper ^= ((state->_integer[i] >> (j + 6)) & 1)*A[57 - j];
            _helper ^= ((state->_integer[i] >> (j + 7)) & 1)*A[56 - j];
        }
        result->_integer[i] = _helper;
        _helper = 0;
    }

}

void KeySchedule(const BigInt_t* K, uint_fast8_t i, BigInt_t* result) {
    XTransformation(K, &C[i], result);
    STransformation(K, result);
    PTransformation(K, result);
    LTransformation(K, result);
}

void E(BigInt_t* K, const BigInt_t* m, BigInt_t* result) {
    XTransformation(K, m, result);
    for (uint_fast8_t i = 0; i < 12; ++i)
    {
        STransformation(result, result);
        PTransformation(result, result);
        LTransformation(result, result);
        KeySchedule(K, i, K);
        XTransformation(result, K, result);
    }
}

void Gn(const BigInt_t* NState, const BigInt_t* h, const BigInt_t* m, BigInt_t* result) {
    XTransformation(h, NState, result);
    STransformation(result, result);
    PTransformation(result, result);
    LTransformation(result, result);
    E(result, m, result);
    XTransformation(result, h, result);
    XTransformation(result, m, result);
}

const BigInt_t* GetHash512(const uint8_t* restrict message, uint_least32_t messageLength) {
    uint_least64_t offset = 0;
    uint_least64_t _len = messageLength;

    static BigInt_t h;
    BigInt_t m;
    BigInt_t paddedMessage;
    BigInt_t N = {._integer = {0, 0, 0, 0, 0, 0, 0, 0}};
    BigInt_t Sigma = {._integer = {0, 0, 0, 0, 0, 0, 0, 0}};

    // STEP 2 - FOLDING
    while (_len >= 64) {
        offset += 8;
        InitFromBytes(&m, &message[offset]);
        Gn(&N, &h, &m, &h);
        AddModulo512(&N, &N512, &N);
        AddModulo512(&Sigma, &m, &Sigma);
        _len -= 64;
    }

    // STEP 3 - PADDING
    if(messageLength - offset < 64) {

    }


    // STEP 4
    Gn(&N, &h, &paddedMessage, &h);
    return &h;
}
