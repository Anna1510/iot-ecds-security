//
// Created by Анна Еловикова on 15.08.2021.
//
#pragma once
#ifndef STRIBOG_BIGINT_H
#define STRIBOG_BIGINT_H
#endif //STRIBOG_BIGINT_H
#include "utils.h"

typedef struct BigInt {
    uint_least64_t _integer[8];
} BigInt_t;

void InitFromString(BigInt_t* target, const char* source);

void InitFromBytes(BigInt_t* target, const uint8_t* source);

uint_least8_t GetByte(const BigInt_t* source, uint_least8_t byteNum);
