//
// Created by Анна Еловикова on 15.08.2021.
//

#include "utils.h"
uint_least64_t char2int(char input) {
    return (input >= '0' && input <= '9')*(input - '0') + (input >= 'a' && input <= 'f')*(input - 'a' + 10);
}