//
// Created by Анна Еловикова on 15.08.2021.
//

#include "BigInt.h"
void InitFromString(BigInt_t* target, const char* restrict source) {
    target->_integer[0] = (char2int(source[0]) << 60)  + (char2int(source[1]) << 56) +
                          (char2int(source[2]) << 52)  + (char2int(source[3]) << 48) +
                          (char2int(source[4]) << 44)  + (char2int(source[5]) << 40) +
                          (char2int(source[6]) << 36)  + (char2int(source[7]) << 32) +
                          (char2int(source[8]) << 28)  + (char2int(source[9]) << 24) +
                          (char2int(source[10]) << 20) + (char2int(source[11]) << 16) +
                          (char2int(source[12]) << 12) + (char2int(source[13]) << 8) +
                          (char2int(source[14]) << 4)  + (char2int(source[15]));

    target->_integer[1] = (char2int(source[16]) << 60) + (char2int(source[17]) << 56) +
                          (char2int(source[18]) << 52) + (char2int(source[19]) << 48) +
                          (char2int(source[20]) << 44) + (char2int(source[21]) << 40) +
                          (char2int(source[22]) << 36) + (char2int(source[23]) << 32) +
                          (char2int(source[24]) << 28) + (char2int(source[25]) << 24) +
                          (char2int(source[26]) << 20) + (char2int(source[27]) << 16) +
                          (char2int(source[28]) << 12) + (char2int(source[29]) << 8) +
                          (char2int(source[30]) << 4)  + (char2int(source[31]));

    target->_integer[2] = (char2int(source[32]) << 60)  + (char2int(source[33]) << 56) +
                          (char2int(source[34]) << 52)  + (char2int(source[35]) << 48) +
                          (char2int(source[36]) << 44)  + (char2int(source[37]) << 40) +
                          (char2int(source[38]) << 36)  + (char2int(source[39]) << 32) +
                          (char2int(source[40]) << 28)  + (char2int(source[41]) << 24) +
                          (char2int(source[42]) << 20) + (char2int(source[43]) << 16) +
                          (char2int(source[44]) << 12) + (char2int(source[45]) << 8) +
                          (char2int(source[46]) << 4)  + (char2int(source[47]));

    target->_integer[3] = (char2int(source[48]) << 60)  + (char2int(source[49]) << 56) +
                          (char2int(source[50]) << 52)  + (char2int(source[51]) << 48) +
                          (char2int(source[52]) << 44)  + (char2int(source[53]) << 40) +
                          (char2int(source[54]) << 36)  + (char2int(source[55]) << 32) +
                          (char2int(source[56]) << 28)  + (char2int(source[57]) << 24) +
                          (char2int(source[58]) << 20) + (char2int(source[59]) << 16) +
                          (char2int(source[60]) << 12) + (char2int(source[61]) << 8) +
                          (char2int(source[62]) << 4)  + (char2int(source[63]));

    target->_integer[4] = (char2int(source[64]) << 60)  + (char2int(source[65]) << 56) +
                          (char2int(source[66]) << 52)  + (char2int(source[67]) << 48) +
                          (char2int(source[68]) << 44)  + (char2int(source[69]) << 40) +
                          (char2int(source[70]) << 36)  + (char2int(source[71]) << 32) +
                          (char2int(source[72]) << 28)  + (char2int(source[73]) << 24) +
                          (char2int(source[74]) << 20) + (char2int(source[75]) << 16) +
                          (char2int(source[76]) << 12) + (char2int(source[77]) << 8) +
                          (char2int(source[78]) << 4)  + (char2int(source[79]));

    target->_integer[5] = (char2int(source[80]) << 60)  + (char2int(source[81]) << 56) +
                          (char2int(source[82]) << 52)  + (char2int(source[83]) << 48) +
                          (char2int(source[84]) << 44)  + (char2int(source[85]) << 40) +
                          (char2int(source[86]) << 36)  + (char2int(source[87]) << 32) +
                          (char2int(source[88]) << 28)  + (char2int(source[89]) << 24) +
                          (char2int(source[90]) << 20) + (char2int(source[91]) << 16) +
                          (char2int(source[92]) << 12) + (char2int(source[93]) << 8) +
                          (char2int(source[94]) << 4)  + (char2int(source[95]));

    target->_integer[6] = (char2int(source[96]) << 60)  + (char2int(source[97]) << 56) +
                          (char2int(source[98]) << 52)  + (char2int(source[99]) << 48) +
                          (char2int(source[100]) << 44)  + (char2int(source[101]) << 40) +
                          (char2int(source[102]) << 36)  + (char2int(source[103]) << 32) +
                          (char2int(source[104]) << 28)  + (char2int(source[105]) << 24) +
                          (char2int(source[106]) << 20) + (char2int(source[107]) << 16) +
                          (char2int(source[108]) << 12) + (char2int(source[109]) << 8) +
                          (char2int(source[110]) << 4)  + (char2int(source[111]));

    target->_integer[7] = (char2int(source[112]) << 60)  + (char2int(source[113]) << 56) +
                          (char2int(source[114]) << 52)  + (char2int(source[115]) << 48) +
                          (char2int(source[116]) << 44)  + (char2int(source[117]) << 40) +
                          (char2int(source[118]) << 36)  + (char2int(source[119]) << 32) +
                          (char2int(source[120]) << 28)  + (char2int(source[121]) << 24) +
                          (char2int(source[122]) << 20) + (char2int(source[123]) << 16) +
                          (char2int(source[124]) << 12) + (char2int(source[125]) << 8) +
                          (char2int(source[126]) << 4)  + (char2int(source[127]));
}

void InitFromBytes(BigInt_t* restrict target, const uint8_t* restrict source) {
    for (int i = 0; i < 8; ++i) {
        uint_least64_t _helper1, _helper2, _helper3, _helper4, _helper5, _helper6, _helper7;
        _helper1 = source[i * 8]; _helper1 <<= 56;
        _helper2 = source[(i * 8) + 1]; _helper2 <<= 48;
        _helper3 = source[(i * 8) + 2]; _helper3 <<= 40;
        _helper4 = source[(i * 8) + 3]; _helper4 <<= 32;
        _helper5 = source[(i * 8) + 4]; _helper5 <<= 24;
        _helper6 = source[(i * 8) + 5]; _helper6 <<= 16;
        _helper7 = source[(i * 8) + 6]; _helper7 <<= 8;
        target->_integer[i] = _helper1 + _helper2 + _helper3 + _helper4 + _helper5 + _helper6 +
                              _helper7 + source[(i * 8) + 7];
    }
}

uint_least8_t GetByte(const BigInt_t* source, uint_least8_t byteNum) {
    return source->_integer[byteNum / 8] >> (byteNum % 8 * 8) & 0xff;
}
